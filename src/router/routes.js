import authGuardFactory from './auth-guard-factory';
import {
	ROUTE_AUTH,
	ROUTE_NOT_FOUND,
	ROUTE_MAIN,
	ROUTE_ALBUMS,
	ROUTE_ALBUMS_VIEW,
	ROUTE_ALBUMS_EDIT,
	ROUTE_ALBUMS_CREATE,
	ROUTE_PLAYLISTS,
	ROUTE_PLAYLISTS_VIEW
} from './route-names';

export default [
	// Auth
	{
		name: ROUTE_AUTH,
		path: '/auth',
		loadComponent: () => import('../pages/auth')
	},

	// Not found
	{
		name: ROUTE_NOT_FOUND,
		path: '/not-found',
		loadComponent: () => import('../pages/not-found')
	},

	// App container for authorized users
	{
		name: ROUTE_MAIN,
		path: '/',
		forwardTo: ROUTE_ALBUMS,
		canActivate: authGuardFactory,
		loadComponent: () => import('../pages/main')
	},

	// Albums module
	{name: ROUTE_ALBUMS, path: 'albums'},
	{name: ROUTE_ALBUMS_VIEW, path: '/view/:id'},
	{name: ROUTE_ALBUMS_EDIT, path: '/edit/:id'},
	{name: ROUTE_ALBUMS_CREATE, path: '/create'},

	// Playlists module
	{name: ROUTE_PLAYLISTS, path: 'playlists'},
	{name: ROUTE_PLAYLISTS_VIEW, path: '/view/:id'}
];
