export const ROUTE_AUTH = 'auth';
export const ROUTE_NOT_FOUND = 'not-found';
export const ROUTE_MAIN = 'main';

export const ROUTE_ALBUMS = `${ROUTE_MAIN}.albums`;
export const ROUTE_ALBUMS_VIEW = `${ROUTE_ALBUMS}.view`;
export const ROUTE_ALBUMS_EDIT = `${ROUTE_ALBUMS}.edit`;
export const ROUTE_ALBUMS_CREATE = `${ROUTE_ALBUMS}.create`;

export const ROUTE_PLAYLISTS = `${ROUTE_MAIN}.playlists`;
export const ROUTE_PLAYLISTS_VIEW = `${ROUTE_PLAYLISTS}.view`;
