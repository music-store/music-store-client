import axios from 'axios';
import {useState, useEffect} from 'react';
import {albumGetById} from '../api/album';

export function useGetAlbum(albumId) {
	const [album, setAlbum] = useState(null);

	useEffect(() => {
		let source = null;

		if (typeof albumId !== 'undefined') {
			source = axios.CancelToken.source();

			albumGetById(albumId, source.token)
				.then(({data}) => {
					setAlbum(data);
				})
				.catch((err) => {
					if (!axios.isCancel(err)) {
						setAlbum(null);
						console.error(err);
					}
				});
		}

		return () => {
			if (Boolean(source) && typeof source.cancel === 'function') {
				source.cancel('Cancel ALBUM_GET request');
			}
		};
	}, [albumId]);

	return album;
}
