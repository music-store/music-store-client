import axios from 'axios';
import {useState, useEffect} from 'react';
import {trackGetList} from '../api/track';

export function useGetTrackList(albumId) {
	const [trackList, setTrackList] = useState([]);

	useEffect(() => {
		let source = null;

		if (typeof albumId !== 'undefined') {
			source = axios.CancelToken.source();

			trackGetList({album_id: albumId}, source.token)
				.then(({data}) => {
					setTrackList(data);
				})
				.catch((err) => {
					console.error(err);
					setTrackList([]);
				});
		}

		return () => {
			if (Boolean(source) && typeof source.cancel === 'function') {
				source.cancel('Cancel TRACK_LIST request');
			}
		};
	}, [albumId]);

	return trackList;
}
