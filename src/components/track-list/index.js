import './track-list.css';
import React from 'react';
import {Button} from 'react-bootstrap';
import TrackCard from '../track-card';

export default function TrackList({tracks, isEditMode, offsetNumber, removeTrack, likeTrack}) {
	const offset = typeof offsetNumber === 'number' ? offsetNumber : 0;
	const remove = typeof removeTrack === 'function' ? removeTrack : () => {};
	const like = typeof likeTrack === 'function' ? likeTrack : () => {};

	return (
		<div className="track-list">
			{
				tracks.map(
					(t, i) => (
						<div key={i} className="track-list__row">
							<TrackCard track={t} number={offset + i + 1}/>
							{
								isEditMode
									? (
										<Button className="track-list__button"
														variant="outline-danger"
														size="sm"
														onClick={() => remove(t, i)}
										>Remove</Button>
									)
									: (
										<Button className="track-list__button"
														variant="outline-success"
														size="sm"
														onClick={() => like(t, i)}
										>Like</Button>
									)
							}
						</div>
					)
				)
			}
		</div>
	);
}
