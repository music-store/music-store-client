import React from 'react';
import {Form} from "react-bootstrap";

const renderOptions = (genres) => Boolean(genres)
	? genres.map((g) => <option key={g.id} value={g.id}>{g.name}</option>)
	: null;

export default function AlbumForm({album, genres, change}) {
	const form = Boolean(album) ? album : {};
	const name = Boolean(album) ? album.name : '';
	const genreId = Boolean(album) ? album.genreId : '';

	return (
		<Form>
			<Form.Group controlId="editAlbumForm.name">
				<Form.Label>Name</Form.Label>
				<Form.Control as="textarea"
											rows="2"
											value={name}
											onChange={(e) => change({...form, name: e.target.value})}/>
			</Form.Group>
			<Form.Group controlId="editAlbumForm.genre">
				<Form.Label>Genre</Form.Label>
				<Form.Control as="select"
											value={genreId}
											onChange={(e) => change({...form, genreId: parseInt(e.target.value, 10)})}>
					{renderOptions(genres)}
				</Form.Control>
			</Form.Group>
		</Form>
	);
}
