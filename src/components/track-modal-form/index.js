import React, {Fragment, useState} from 'react';
import {Button, Form, Modal} from 'react-bootstrap';

const initialState = {name: '', url: '', duration: 0};

export default function TrackModalForm({isShow, onClose}) {
	const [form, setForm] = useState(initialState);

	const onChange = (e) =>{
		const {name, value} = e.target;
		const preparedValue = name === 'duration'
			? (parseInt(value, 10) || 0)
			: value;

		setForm({...form, [name]: preparedValue});
	};

	const resetForm = () => {
		setForm(initialState);
	};

	const close = (value = null) => () => {
		onClose(value);
		resetForm();
	};

	return (
		<Fragment>
			<Modal show={isShow} onHide={close()}>
				<Modal.Header closeButton>
					<Modal.Title>Create track</Modal.Title>
				</Modal.Header>

				<Modal.Body>
					<Form>
						<Form.Group controlId="editTrackForm.name">
							<Form.Label>Name</Form.Label>
							<Form.Control as="input"
														type="text"
														name="name"
														required
														value={form.name}
														onChange={onChange}/>
						</Form.Group>
						<Form.Group controlId="editTrackForm.url">
							<Form.Label>URL</Form.Label>
							<Form.Control as="input"
														type="text"
														name="url"
														required
														value={form.url}
														onChange={onChange} />
						</Form.Group>
						<Form.Group controlId="editTrackForm.duration">
							<Form.Label>Duration</Form.Label>
							<Form.Control as="input"
														type="number"
														name="duration"
														min={0}
														required
														value={form.duration}
														onChange={onChange} />
						</Form.Group>
					</Form>
				</Modal.Body>

				<Modal.Footer>
					<Button variant="primary" onClick={close(form)}>Save</Button>
					<Button variant="outline-danger" onClick={close()}>Cancel</Button>
				</Modal.Footer>
			</Modal>
		</Fragment>
	);
}
