import React from 'react';
import {Button} from "react-bootstrap";
import {Link} from 'react-router5';
import './album-edit-header.css';

import {
	ROUTE_ALBUMS_VIEW,
	ROUTE_ALBUMS_EDIT,
	ROUTE_ALBUMS_CREATE
} from '../../router/route-names';

const getTitle = (routeName, album) => {
	switch (routeName) {
		case ROUTE_ALBUMS_VIEW:
			return Boolean(album) ? album.name : '';
		case ROUTE_ALBUMS_EDIT:
			return 'Edit album';
		case ROUTE_ALBUMS_CREATE:
			return 'Create album';
		default:
			return '';
	}
};

const renderGenre = (routeName, album, genres) => {
	const isAvailable = routeName === ROUTE_ALBUMS_VIEW
		&& Boolean(album)
		&& Boolean(album.genreId)
		&& Boolean(genres)
		&& Boolean(genres.length);

	if (!isAvailable) {
		return null;
	}

	const genre = genres.find((g) => g.id === album.genreId);

	return (
		<span className="album-edit-header__genre">({
			Boolean(genre) ? genre.name : 'Genre name'
		})</span>
	);
};

const renderEditBtn = (routeName, album) => {
	if (routeName !== ROUTE_ALBUMS_VIEW || !Boolean(album) || !Boolean(album.id)) {
		return null;
	}

	return (
		<Link routeName={ROUTE_ALBUMS_EDIT}
					routeParams={{id: album.id}}
					className="album-edit-header__edit-btn">
			<Button variant="primary" as="span">Edit album</Button>
		</Link>
	);
};

export default function AlbumEditHeader({routeName, album, genres}) {
	return (
		<div className="album-edit-header">
			<h1>{ getTitle(routeName, album) }</h1>
			{ renderGenre(routeName, album, genres) }
			{ renderEditBtn(routeName, album) }
		</div>
	);
}
