import React from 'react';
import {Card} from 'react-bootstrap';

export default function AlbumCard({album}) {
	return (
		<Card style={{ width: '280px' }}>
			<Card.Img variant="top" src="https://via.placeholder.com/280x180" />
			<Card.Body>
				<Card.Title>{album.name}</Card.Title>
			</Card.Body>
		</Card>
	);
}
