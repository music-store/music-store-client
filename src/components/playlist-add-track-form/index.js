import React, {Fragment, useCallback, useEffect, useState} from 'react';
import {Button, Form, Modal} from "react-bootstrap";

const initialState = {id: 'null', playlist: null, isValid: false};

export default function PlaylistAddTrackForm({track, playlists, isShow, onClose}) {
	const [state, setState] = useState({...initialState});

	const handleChange = useCallback((e) => {
		const id = parseInt(e.target.value, 10) || null;

		if (!Boolean(id)) {
			return setState({...initialState});
		}

		const playlist = playlists.find((p) => p.id === id);
		setState({id, playlist, isValid: true});
	}, [playlists]);

	/** Сбросить состояние на начальное при закрытии формы */
	useEffect(() => { !isShow && setState({...initialState}) }, [isShow]);

	return (
		<Fragment>
			<Modal show={isShow} onHide={() => onClose(null)}>
				<Modal.Header closeButton>
					<Modal.Title>Add track to playlist</Modal.Title>
				</Modal.Header>

				<Modal.Body>
					<p>{Boolean(track) && track.name}</p>
					<Form>
						<Form.Group controlId="addTrackToPlaylistForm.playlist">
							<Form.Label>Playlist</Form.Label>
							<Form.Control as="select"
														name="playlist"
														required
														value={state.id}
														onChange={handleChange}>
								<option disabled={true} value={'null'}>Select playlist</option>
								{ Boolean(playlists) && playlists.map((p) => <option key={p.id} value={p.id}>{p.name}</option>) }
							</Form.Control>
						</Form.Group>
					</Form>
				</Modal.Body>

				<Modal.Footer>
					<Button variant="primary" disabled={!state.isValid} onClick={() => onClose(state.playlist)}>Save</Button>
					<Button variant="outline-danger" onClick={() => onClose(null)}>Cancel</Button>
				</Modal.Footer>
			</Modal>
		</Fragment>
	);
}
