import React from 'react';
import './track-card.css';

const prepare = (num) => num > 9 ? num : `0${num}`;

const convertDuration = (seconds) => {
	const rest = seconds % 60;
	const minutes = (seconds - rest) / 60;

	return `${prepare(minutes)}:${prepare(rest)}`;
};

export default function TrackCard({track, number}) {
	return (
		<div className="track-card">
			<div className="track-card__col">
				<span className="track-card__number">{number}.</span>
			</div>
			<div className="track-card__col">
				<a href={track.url} target="_blank" rel="noopener noreferrer" className="track-card__link">{track.name}</a>
			</div>
			<div className="track-card__col">
				<span className="track-card__duration">{convertDuration(track.duration)}</span>
			</div>
		</div>
	);
}
