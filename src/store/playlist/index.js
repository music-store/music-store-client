import {playlistGetList, playlistSave, playlistGetById, playlistRemove} from '../../api/playlist';
import {PLAYLIST_STATE_NAME} from './state-name';
import {
	PLAYLIST_LIST_FETCH,
	PLAYLIST_LIST_SET,
	PLAYLIST_ITEM_SAVE,
	PLAYLIST_ITEM_SET,
	PLAYLIST_ITEM_FETCH,
	PLAYLIST_ITEM_REMOVE
} from './action-names';

const initialState = {
	[PLAYLIST_STATE_NAME]: {
		list: [],
		item: null
	}
};

export default (store) => {
	store.on('@init', () => initialState);

	store.on(PLAYLIST_LIST_SET, (state, playlists) => {
		return {
			[PLAYLIST_STATE_NAME]: {...state[PLAYLIST_STATE_NAME], list: playlists}
		};
	});

	store.on(PLAYLIST_ITEM_SET, (state, playlist) => {
		return {
			[PLAYLIST_STATE_NAME]: {...state[PLAYLIST_STATE_NAME], item: playlist}
		};
	});

	store.on(PLAYLIST_LIST_FETCH, async () => {
		try {
			const {data: playlists} = await playlistGetList();
			store.dispatch(PLAYLIST_LIST_SET, playlists);
		} catch (err) {
			console.error(err);
		}
	});

	store.on(PLAYLIST_ITEM_SAVE, async (_, playlist) => {
		try {
			const {data: updated} = await playlistSave(playlist);
			store.dispatch(PLAYLIST_ITEM_SET, updated);
		} catch (err) {
			console.error(err);
		}
	});

	store.on(PLAYLIST_ITEM_FETCH, async (_, id) => {
		try {
			const {data: playlist} = await playlistGetById(id);
			store.dispatch(PLAYLIST_ITEM_SET, playlist);
		} catch (err) {
			console.error(err);
		}
	});

	store.on(PLAYLIST_ITEM_REMOVE, async (_, id) => {
		try {
			await playlistRemove(id);
			store.dispatch(PLAYLIST_ITEM_SET, null);
		} catch (err) {
			console.error(err);
		}
	});
};
