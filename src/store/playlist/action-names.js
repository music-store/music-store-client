import {PLAYLIST_STATE_NAME} from './state-name';

export const PLAYLIST_LIST_FETCH = `${PLAYLIST_STATE_NAME}/list/fetch`;
export const PLAYLIST_LIST_SET = `${PLAYLIST_STATE_NAME}/list/set`;

export const PLAYLIST_ITEM_FETCH = `${PLAYLIST_STATE_NAME}/item/fetch`;
export const PLAYLIST_ITEM_SAVE = `${PLAYLIST_STATE_NAME}/item/save`;
export const PLAYLIST_ITEM_SET = `${PLAYLIST_STATE_NAME}/item/set`;
export const PLAYLIST_ITEM_REMOVE = `${PLAYLIST_STATE_NAME}/item/remove`;
