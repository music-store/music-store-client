import createStore from 'storeon';
import auth from './auth';
import profile from './profile';
import genre from './genre';
import album from './album';
import playlist from './playlist';
import track from './track';

export default createStore([
	auth,
	profile,
	genre,
	album,
	playlist,
	track,
	process.env.NODE_ENV !== 'production' && require('storeon/devtools')
]);
