import {GENRE_STATE_NAME} from './state-name';

export const GENRE_FETCH = `${GENRE_STATE_NAME}/fetch`;
export const GENRE_SET = `${GENRE_STATE_NAME}/set`;
