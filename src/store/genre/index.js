import {GENRE_STATE_NAME} from './state-name';
import {GENRE_FETCH, GENRE_SET} from './action-names';
import {genreGetList} from '../../api/genre';

const initialState = {
	[GENRE_STATE_NAME]: []
};

export default (store) => {
	store.on('@init', () => initialState);

	store.on(GENRE_SET, (_, genres) => {
		return {[GENRE_STATE_NAME]: genres};
	});

	store.on(GENRE_FETCH, async () => {
		try {
			const {data: genres} = await genreGetList();
			store.dispatch(GENRE_SET, genres);
		} catch (err) {
			console.error(err);
		}
	});
};
