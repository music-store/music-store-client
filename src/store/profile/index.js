import {PROFILE_STATE_NAME} from './state-name';

const initialState = {
	[PROFILE_STATE_NAME]: {name: 'Vofus'}
};

export default (store) => {
	store.on('@init', () => initialState);
};
