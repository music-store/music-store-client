import {trackGetList} from '../../api/track';
import {TRACK_STATE_NAME} from './state-name';
import {TRACK_LIST_FETCH, TRACK_LIST_SET} from './action-names';

const initialState = {
	[TRACK_STATE_NAME]: {
		list: [],
		item: null
	}
};

export default (store) => {
	store.on('@init', () => initialState);

	store.on(TRACK_LIST_SET, (state, tracks) => {
		return {
			[TRACK_STATE_NAME]: {...state[TRACK_STATE_NAME], list: tracks}
		};
	});

	store.on(TRACK_LIST_FETCH, async (_, params) => {
		try {
			const {data: tracks} = await trackGetList(params);
			store.dispatch(TRACK_LIST_SET, tracks);
		} catch (err) {
			console.error(err);
		}
	});
};
