import {TRACK_STATE_NAME} from './state-name'

export const TRACK_LIST_FETCH = `${TRACK_STATE_NAME}/list/fetch`;
export const TRACK_LIST_SET = `${TRACK_STATE_NAME}/list/set`;

export const TRACK_ITEM_FETCH = `${TRACK_STATE_NAME}/item/fetch`;
export const TRACK_ITEM_SAVE = `${TRACK_STATE_NAME}/item/save`;
export const TRACK_ITEM_SET = `${TRACK_STATE_NAME}/item/set`;
