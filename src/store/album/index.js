import {albumGetList, albumGetById} from '../../api/album';
import {ALBUM_STATE_NAME} from './state-name';
import {
	ALBUM_LIST_FETCH,
	ALBUM_LIST_SET,
	ALBUM_ITEM_FETCH,
	ALBUM_ITEM_SET
} from './action-names';

const initialState = {
	[ALBUM_STATE_NAME]: {
		list: [],
		item: null
	}
};

export default (store) => {
	store.on('@init', () => initialState);

	store.on(ALBUM_LIST_SET, (state, albums) => {
		return {
			[ALBUM_STATE_NAME]: {...state[ALBUM_STATE_NAME], list: albums}
		};
	});

	store.on(ALBUM_ITEM_SET, (state, album) => {
		return {
			[ALBUM_STATE_NAME]: {...state[ALBUM_STATE_NAME], item: album}
		};
	});

	store.on(ALBUM_LIST_FETCH, async () => {
		try {
			const {data: albums} = await albumGetList();
			store.dispatch(ALBUM_LIST_SET, albums);
		} catch (err) {
			console.error(err);
		}
	});

	store.on(ALBUM_ITEM_FETCH, async (_, albumId) => {
		try {
			const {data: album} = await albumGetById(albumId);
			store.dispatch(ALBUM_ITEM_SET, album);
		} catch (err) {
			console.error(err);
		}
	});
};
