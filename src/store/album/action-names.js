import {ALBUM_STATE_NAME} from './state-name';

export const ALBUM_LIST_FETCH = `${ALBUM_STATE_NAME}/list/fetch`;
export const ALBUM_LIST_SET = `${ALBUM_STATE_NAME}/list/set`;
export const ALBUM_ITEM_FETCH = `${ALBUM_STATE_NAME}/item/fetch`;
export const ALBUM_ITEM_SET = `${ALBUM_STATE_NAME}/item/set`;
