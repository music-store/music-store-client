import api from './axios-module';
const URL = '/albums';

export function albumGetList() {
	return api.get(URL);
}

export function albumGetById(id, cancelToken) {
	return api.get(`${URL}/${id}`, {cancelToken});
}

export function albumSave(album, cancelToken) {
	if (Boolean(album.id)) {
		return api.put(`${URL}/${album.id}`, album, {cancelToken});
	}

	return api.post(URL, album, {cancelToken});
}
