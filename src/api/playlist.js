import api from './axios-module';
const URL = '/playlists';

export function playlistGetList() {
	return api.get(URL);
}

export function playlistGetById(id, cancelToken) {
	return api.get(`${URL}/${id}`, {cancelToken});
}

export function playlistSave(playlist, cancelToken) {
	if (Boolean(playlist.id)) {
		return api.put(`${URL}/${playlist.id}`, playlist, {cancelToken});
	}

	return api.post(URL, playlist, {cancelToken});
}

export function playlistRemove(id) {
	return api.delete(`${URL}/${id}`);
}
