import api from './axios-module';

export async function genreGetList() {
	return api.get('/genres');
}
