import api from './axios-module';
const URL = '/tracks';

export function trackGetList(params = {}, cancelToken) {
	return api.get(URL, {params, cancelToken});
}

export function trackSave(track, cancelToken) {
	if (Boolean(track.id)) {
		return api.put(`${URL}/${track.id}`, track, {cancelToken});
	}

	return api.post(URL, track, {cancelToken});
}

export function trackRemove(id) {
	return api.delete(`${URL}/${id}`);
}
