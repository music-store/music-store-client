export const GENRES = [
	{id: 1, name: 'Рок'},
	{id: 2, name: 'Поп'},
	{id: 3, name: 'Рэп'},
	{id: 4, name: 'Диско'},
	{id: 5, name: 'Шансон'}
];
