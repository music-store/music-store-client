import MockAdapter from 'axios-mock-adapter';
import {GENRES} from './data/genres';
import {ALBUMS} from './data/albums';
import {TRACKS} from './data/tracks';
import {PLAYLISTS} from './data/playlists';

const ALBUM_ID_PATTERN = /(\/albums\/)(\d+)$/;
const TRACK_ID_PATTERN = /(\/tracks\/)(\d+)$/;
const PLAYLIST_ID_PATTERN = /(\/playlists\/)(\d+)$/;
const mock = new MockAdapter();

// Config delay
mock.delayResponse = 200;

// Genres
mock.onGet('/genres').reply(200, GENRES);

// Albums
mock.onGet('/albums').reply(200, ALBUMS);
mock.onGet(ALBUM_ID_PATTERN).reply((config) => {
	const matches = config.url.match(ALBUM_ID_PATTERN);
	const id = Boolean(matches) ? parseInt(matches[matches.length - 1], 10) : 0;
	const album = !isNaN(id) && id > 0 ? ALBUMS[id-1] : null;

	return [200, album];
});
mock.onPut(ALBUM_ID_PATTERN).reply((config) => [200, config.data]);
mock.onPost('/albums').reply((config) => {
	const data = typeof config.data === 'string' ? JSON.parse(config.data) : config.data;
	const album = {...data, id: ALBUMS.length + 1};
	ALBUMS.push(album);

	return [200, album];
});

// Playlists
mock.onGet('/playlists').reply(200, PLAYLISTS);
mock.onGet(PLAYLIST_ID_PATTERN).reply((config) => {
	const matches = config.url.match(PLAYLIST_ID_PATTERN);
	const id = Boolean(matches) ? parseInt(matches[matches.length - 1], 10) : 0;
	const playlist = !isNaN(id) && id > 0 ? PLAYLISTS[id-1] : null;

	return [200, playlist];
});
mock.onPut(PLAYLIST_ID_PATTERN).reply((config) => {
	const data = typeof config.data === 'string' ? JSON.parse(config.data) : config.data;
	const matches = config.url.match(PLAYLIST_ID_PATTERN);
	const id = Boolean(matches) ? parseInt(matches[matches.length - 1], 10) : 0;

	if (!isNaN(id) && id > 0) {
		let index = PLAYLISTS.length;
		let flag = false;

		while (index--) {
			if (PLAYLISTS[index].id === id) {
				flag = true;
				break;
			}
		}

		if (flag) {
			PLAYLISTS.splice(index, 1, data);
		}
	}

	return [200, data];
});
mock.onDelete(PLAYLIST_ID_PATTERN).reply((config) => {
	const matches = config.url.match(PLAYLIST_ID_PATTERN);
	const id = Boolean(matches) ? parseInt(matches[matches.length - 1], 10) : 0;

	if (!isNaN(id) && id > 0) {
		let index = PLAYLISTS.length;
		let flag = false;

		while (index--) {
			if (PLAYLISTS[index].id === id) {
				flag = true;
				break;
			}
		}

		if (flag) {
			PLAYLISTS.splice(index, 1);
		}
	}

	return [200];
});
mock.onPost('/playlists').reply((config) => {
	const data = typeof config.data === 'string' ? JSON.parse(config.data) : config.data;
	const playlist = {...data, id: PLAYLISTS.length + 1};
	PLAYLISTS.push(playlist);

	return [200, playlist];
});

// Tracks
mock.onGet('/tracks').reply(200, TRACKS);
mock.onPost('/tracks').reply((config) => {
	const data = typeof config.data === 'string' ? JSON.parse(config.data) : config.data;
	const track = {...data, id: TRACKS.length + 1};
	TRACKS.push(track);

	return [200, track];
});
mock.onDelete(TRACK_ID_PATTERN).reply((config) => {
	const matches = config.url.match(TRACK_ID_PATTERN);
	const id = Boolean(matches) ? parseInt(matches[matches.length - 1], 10) : 0;

	if (!isNaN(id)) {
		let index = TRACKS.length;
		let flag = false;

		while (index--) {
			if (TRACKS[index].id === id) {
				flag = true;
				break;
			}
		}

		if (flag) {
			TRACKS.splice(index, 1);
		}
	}

	return [200];
});

export default mock.adapter();
