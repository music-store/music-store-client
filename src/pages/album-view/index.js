import './album-view.css';
import React, {useState, useCallback, useEffect} from 'react';
import {useRoute} from 'react-router5';
import {useGetAlbum, useGetTrackList} from '../../effects';
import AlbumEditHeader from '../../components/album-edit-header';
import TrackList from '../../components/track-list';
import PlaylistAddTrackForm from '../../components/playlist-add-track-form';
import {playlistSave} from '../../api/playlist';

import useStoreon from 'storeon/react';
import {GENRE_STATE_NAME} from '../../store/genre/state-name';
import {PLAYLIST_STATE_NAME} from '../../store/playlist/state-name';
import {PLAYLIST_LIST_FETCH} from '../../store/playlist/action-names';

export default function AlbumView() {
	const {route} = useRoute();
	const album = useGetAlbum(route.params.id);
	const tracks = useGetTrackList(route.params.id);
	const {
		dispatch,
		[GENRE_STATE_NAME]: genres,
		[PLAYLIST_STATE_NAME]: playlists
	} = useStoreon(GENRE_STATE_NAME, PLAYLIST_STATE_NAME);

	// Inner state
	const [state, setState] = useState({
		track: null,
		loadPlaylists: 1
	});

	const addTrackToPlaylist = useCallback((playlist) => {
		if (!Boolean(state.track) || !Boolean(playlist)) {
			return setState({...state, track: null});
		}

		(playlist.trackIds.indexOf(state.track.id) < 0
				? playlistSave({...playlist, trackIds: [...playlist.trackIds, state.track.id]})
				: Promise.resolve()
		).finally(() => setState({track: null, loadPlaylists: state.loadPlaylists + 1}));
	}, [state]);

	/** Загрузить плэйлисты при старте экрана */
	useEffect(() => { dispatch(PLAYLIST_LIST_FETCH) }, [state.loadPlaylists]);

	return (
		<div className='album-view'>
			<AlbumEditHeader routeName={route.name} album={album} genres={genres}/>

			<div className='album-view__container'>
				<TrackList className='album-view__track-list'
									 tracks={tracks}
									 likeTrack={(t) => setState({...state, track: t})}/>
			</div>

			<PlaylistAddTrackForm track={state.track}
														playlists={playlists.list}
														isShow={Boolean(state.track)}
														onClose={addTrackToPlaylist}/>
		</div>
	);
}
