import './album-list.css';
import React, {useEffect} from 'react';
import {Button} from 'react-bootstrap';
import {Link} from 'react-router5';
import AlbumCard from '../../components/album-card';

import {ROUTE_ALBUMS_VIEW} from '../../router/route-names';

import useStoreon from 'storeon/react';
import {ALBUM_STATE_NAME} from '../../store/album/state-name';
import {ALBUM_LIST_FETCH} from '../../store/album/action-names';

function renderAlbums(albumState) {
	const isAvailable = Boolean(albumState)
		&& Boolean(albumState.list)
		&& Boolean(albumState.list.length);

	if (!isAvailable) {
		return null;
	}

	return albumState.list.map((album) => (
		<Link routeName={ROUTE_ALBUMS_VIEW}
					routeParams={{id: album.id}}
					key={album.id.toString()}
					className="album-list__card">
			<AlbumCard album={album} />
		</Link>
	));
}

export default function AlbumList() {
	const {dispatch, [ALBUM_STATE_NAME]: albumState} = useStoreon(ALBUM_STATE_NAME);

	useEffect(() => {
		dispatch(ALBUM_LIST_FETCH);
	}, [dispatch]);

	return (
		<div className="album-list">

			<div className="album-list__header">
				<h1>Albums</h1>
				<Link routeName="main.albums.create">
					<Button variant="primary" as="span">Create album</Button>
				</Link>
			</div>

			<div className="album-list__container">
				{ renderAlbums(albumState) }
			</div>
		</div>
	);
}
