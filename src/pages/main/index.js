import React, {useCallback, Fragment, useEffect} from 'react';
import {useRoute, Link} from 'react-router5';
import {Button, Navbar, Nav, Image} from "react-bootstrap";
import AlbumList from '../album-list';
import AlbumEdit from '../album-edit';
import AlbumView from '../album-view';
import Playlist from '../playlist';
import PlaylistView from '../playlist-view';
import './main.css';

import {
	ROUTE_AUTH,
	ROUTE_ALBUMS,
	ROUTE_ALBUMS_VIEW,
	ROUTE_ALBUMS_EDIT,
	ROUTE_ALBUMS_CREATE,
	ROUTE_PLAYLISTS, ROUTE_PLAYLISTS_VIEW
} from '../../router/route-names';

import useStoreon from 'storeon/react';
import {AUTH_STATE_NAME} from '../../store/auth/state-name';
import {AUTH_STATE_SIGN_OUT} from '../../store/auth/action-names';
import {GENRE_FETCH} from '../../store/genre/action-names';

const renderPage = (routeName) => {
	switch (routeName) {
		case ROUTE_ALBUMS:
			return <AlbumList />;
		case ROUTE_ALBUMS_VIEW:
			return <AlbumView />;
		case ROUTE_ALBUMS_EDIT:
		case ROUTE_ALBUMS_CREATE:
			return <AlbumEdit />;
		case ROUTE_PLAYLISTS:
			return <Playlist />;
		case ROUTE_PLAYLISTS_VIEW:
			return <PlaylistView />;
		default:
			return null;
	}
};

export default function Main() {
	const {router, route} = useRoute();
	const {dispatch, [AUTH_STATE_NAME]: auth} = useStoreon(AUTH_STATE_NAME);

	const logout = useCallback(() => {
		dispatch(AUTH_STATE_SIGN_OUT);
	}, [dispatch]);

	useEffect(() => {
		if (!Boolean(auth.user)) {
			router.navigate(ROUTE_AUTH);
		}
	}, [auth, router]);

	useEffect(() => {
		dispatch(GENRE_FETCH);
	}, [dispatch]);

	return (
		<Fragment>
			<Navbar bg="dark" variant="dark" style={{padding: '8px 20px'}}>
				<Navbar.Brand href="/" title="Music Store">
					<Image src="/icons/android-icon-48x48.png" alt="Logo of Music Store"/>
				</Navbar.Brand>
				<Nav className="mr-auto">
					<Link routeName={ROUTE_ALBUMS} className="nav-link">Albums</Link>
					<Link routeName={ROUTE_PLAYLISTS} className="nav-link">Playlists</Link>
				</Nav>
				<div className="nav-bar__profile">
					{/*<Image src="https://via.placeholder.com/48x48" roundedCircle/>*/}
					<span className="nav-bar__user-name">{Boolean(auth) && Boolean(auth.user) ? auth.user.name : ''}</span>
					<Button size="sm" variant="outline-light" onClick={logout}>Logout</Button>
				</div>
			</Navbar>
			{
				renderPage(route.name)
			}
		</Fragment>
	);
}
