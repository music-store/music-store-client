import './playlist.css';
import React, {useCallback, useEffect, useState} from 'react';
import {Link} from "react-router5";
import {Button} from "react-bootstrap";
import AlbumCard from '../../components/album-card';
import PlaylistCreateForm from '../../components/playlist-create-form';
import {playlistSave} from '../../api/playlist';

import {ROUTE_PLAYLISTS_VIEW} from '../../router/route-names';

import useStoreon from 'storeon/react';
import {PLAYLIST_STATE_NAME} from '../../store/playlist/state-name';
import {PLAYLIST_LIST_FETCH} from '../../store/playlist/action-names';

function renderCards(playlistState) {
	const isAvailable = Boolean(playlistState)
		&& Boolean(playlistState.list)
		&& Boolean(playlistState.list.length);

	if (!isAvailable) {
		return null;
	}

	return playlistState.list.map((playlist) => (
		<Link routeName={ROUTE_PLAYLISTS_VIEW}
					routeParams={{id: playlist.id}}
					key={playlist.id.toString()}
					className="playlist__card">
			<AlbumCard album={playlist} />
		</Link>
	));
}

export default function Playlist() {
	const {dispatch, [PLAYLIST_STATE_NAME]: playlistState} = useStoreon(PLAYLIST_STATE_NAME);
	const [state, setState] = useState({
		showModal: false,
		loadPlaylist: 1
	});

	const createPlaylist = useCallback((name) => {
		if (Boolean(name)) {
			return playlistSave({name, trackIds: []})
				.finally(() => setState({showModal: false, loadPlaylist: state.loadPlaylist + 1}))
		}

		setState({...state, showModal: false});
	}, [state]);

	/** Загрузить плэйлисты */
	useEffect(() => { dispatch(PLAYLIST_LIST_FETCH) }, [state.loadPlaylist]);

	return (
		<div className="playlist">

			<div className="playlist__header">
				<h1>Playlist</h1>
				<Button variant="primary" onClick={() => setState({...state, showModal: true})}>Create playlist</Button>
			</div>

			<div className="playlist__container">
				{ renderCards(playlistState) }
			</div>

			<PlaylistCreateForm isShow={state.showModal} onClose={createPlaylist} />
		</div>
	);
}
