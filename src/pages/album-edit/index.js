import './album-edit.css';
import React, {useCallback, useEffect, useState} from 'react';
import {useRoute} from 'react-router5';
import {Button} from 'react-bootstrap';
import {albumSave} from '../../api/album';
import {trackSave, trackRemove} from '../../api/track';
import {useGetAlbum, useGetTrackList} from '../../effects';
import TrackList from '../../components/track-list';
import AlbumForm from '../../components/album-form';
import TrackModalForm from '../../components/track-modal-form';
import AlbumEditHeader from '../../components/album-edit-header';

import {
	ROUTE_ALBUMS,
	ROUTE_ALBUMS_VIEW,
	ROUTE_ALBUMS_EDIT,
	ROUTE_ALBUMS_CREATE
} from '../../router/route-names';

import useStoreon from 'storeon/react';
import {GENRE_STATE_NAME} from '../../store/genre/state-name';

const saveAlbum = (album, newTracks, tracksForRemove) => {
	return albumSave(album)
		.then(({data: a}) => {
			return Promise.all(newTracks.map((t) => (t.albumId = a.id, trackSave(t))));
		})
		.then(() => {
			return Promise.all(tracksForRemove.map((t) => trackRemove(t.id)));
		});
};

export default function AlbumEdit() {
	const {route, router} = useRoute();
	const album = useGetAlbum(route.params.id);
	const fetchedTracks = useGetTrackList(route.params.id);
	const {[GENRE_STATE_NAME]: genres} = useStoreon(GENRE_STATE_NAME);

	// Inner State
	const [state, setState] = useState({
		isSubmittingForm: false,
		isShowModal: false,
		form: Boolean(album) ? {...album} : null,
		tracks: [...fetchedTracks],
		newTracks: [],
		removingTracks: []
	});

	/** Перейти к предыдущему экрану */
	const goBack = useCallback(() => {
		if (route.name === ROUTE_ALBUMS_EDIT) {
			router.navigate(ROUTE_ALBUMS_VIEW, {id: route.params.id});
		} else if (route.name === ROUTE_ALBUMS_CREATE) {
			router.navigate(ROUTE_ALBUMS);
		}
	}, [route, router]);

	/** Инициировать эффект сохранения альбома */
	const onSave = useCallback(() => {
		setState({...state, isSubmittingForm: !state.isSubmittingForm});
	}, [state]);

	/** Добавить в массив новых трэков новый трэк */
	const onCloseModal = useCallback((newTrack) => {
		setState({
			...state,
			isShowModal: !state.isShowModal,
			newTracks: Boolean(newTrack) ? [...state.newTracks, newTrack] : state.newTracks
		});
	}, [state]);

	/** Удаление трэка */
	const onRemoveTrack = useCallback((track, index) => {
		if (!Boolean(track.id)) {
			state.newTracks.splice(index, 1);
			setState({...state, newTracks: [...state.newTracks]});
		} else {
			setState({
				...state,
				removingTracks: [...state.removingTracks, ...state.tracks.splice(index, 1)],
				tracks: [...state.tracks]
			});
		}
	}, [state]);

	/** Обновить состояние формы при изменении альбома */ //eslint-disable-next-line
	useEffect(() => { setState({...state, form: Boolean(album) ? {...album} : null}) }, [album]);
	/** Обновить состояние списка трэков в зависимости от загруженных */ //eslint-disable-next-line
	useEffect(() => { setState({...state, tracks: [...fetchedTracks]}) }, [fetchedTracks]);
	/** Сохранить изменения и перейти на предыдущий экран */
	useEffect(() => {
		if (state.isSubmittingForm) {
			const {form, newTracks, removingTracks} = state;

			saveAlbum(form, newTracks, removingTracks)
				.catch(console.error)
				.finally(goBack);
		}
	}, [state.isSubmittingForm]);

	return (
		<div className="album-edit">

			<AlbumEditHeader routeName={route.name} album={album} genres={genres}/>

			<div className="album-edit__container">
				<AlbumForm album={state.form} genres={genres} change={(f) => setState({...state, form: f})}/>
				<TrackList className="album-edit__track-list"
									 tracks={state.tracks}
									 isEditMode={true}
									 removeTrack={onRemoveTrack}
				/>
				<TrackList className="album-edit__track-list"
									 tracks={state.newTracks}
									 offsetNumber={state.tracks.length}
									 isEditMode={true}
									 removeTrack={onRemoveTrack}
				/>
			</div>

			<div className="album-edit__controls-row">
				<Button className="album-edit__control"
								variant="outline-primary"
								onClick={() => setState({...state, isShowModal: true})}>Add track</Button>
				<Button className="album-edit__control" variant="primary" onClick={onSave}>Save</Button>
				<Button className="album-edit__control" variant="outline-danger" onClick={goBack}>Cancel</Button>
			</div>

			<TrackModalForm isShow={state.isShowModal} onClose={onCloseModal}/>

		</div>
	);
}
