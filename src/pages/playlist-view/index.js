import './playlist-view.css';
import React, {useCallback, useEffect, useState} from 'react';
import {Button} from "react-bootstrap";

import {useRoute} from 'react-router5';
import {ROUTE_PLAYLISTS} from '../../router/route-names';

import useStoreon from 'storeon/react';
import {PLAYLIST_STATE_NAME} from '../../store/playlist/state-name';
import {PLAYLIST_ITEM_FETCH, PLAYLIST_ITEM_SET, PLAYLIST_ITEM_REMOVE} from '../../store/playlist/action-names';
import {TRACK_STATE_NAME} from '../../store/track/state-name';
import {TRACK_LIST_FETCH, TRACK_LIST_SET} from '../../store/track/action-names';
import TrackCard from "../../components/track-card";

export default function PlaylistView() {
	const {route, router} = useRoute();
	const [removed, setRemoved] = useState(false);
	const {
		dispatch,
		[PLAYLIST_STATE_NAME]: {item: playlist},
		[TRACK_STATE_NAME]: {list: tracks}
	} = useStoreon(PLAYLIST_STATE_NAME, TRACK_STATE_NAME);

	/** Удалить плэйлист */
	const removeHandler = useCallback(() => {
		dispatch(PLAYLIST_ITEM_REMOVE, playlist.id);
		setRemoved(true);
	}, [playlist]);

	/** Загрузить плэйлист по id */
	useEffect(() => {
		if (Boolean(route.params.id)) {
			dispatch(PLAYLIST_ITEM_FETCH, route.params.id);
		}

		return () => dispatch(PLAYLIST_ITEM_SET, null);
	}, [route.params.id]);

	/** Загрузить список трэков из плэйлиста */
	useEffect(() => {
		const isAvailable = Boolean(playlist)
			&& Boolean(playlist.trackIds)
			&& Boolean(playlist.trackIds.length);

		if (isAvailable) {
			dispatch(TRACK_LIST_FETCH, {ids: playlist.trackIds.join(',')});
		}

		return () => dispatch(TRACK_LIST_SET, []);
	}, [playlist]);

	/** Вернуться на предыдущий экран */
	useEffect(() => {
		if (removed && !Boolean(playlist)) {
			router.navigate(ROUTE_PLAYLISTS);
		}
	}, [removed, playlist]);

	return (
		<div className="playlist-view">

			<div className="playlist-view__header">
				<h1>{Boolean(playlist) && playlist.name}</h1>
				<Button className="playlist-view__header-remove-btn"
								variant="danger"
								onClick={removeHandler}>Remove playlist</Button>
			</div>

			<div className="playlist-view__container">
				<div className="playlist-view__track-list">
					{
						tracks.map(
							(t, i) => (
								<div key={i} className="playlist-view__track-list-row">
									<TrackCard track={t} number={i + 1}/>
								</div>
							)
						)
					}
				</div>
			</div>
		</div>
	);
}
